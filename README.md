# REAPI rust bindings

This crete provides bindings and documentation for the [REAPI](https://github.com/bazelbuild/remote-apis)

## Features

With no features this create builds bindings for the full v2 reapi including the asset api

By default the following features are included:
* Feature `EQ` adds `eq` and `hash` to `build.bazel.remote.execution.v2.Digest`
* Feature `Serde` adds `Serialize` and `Deserialize` to `build.bazel.remote.execution.v2.Digest`
* Feature `buildgrid` adds `build.buildgrid` which provides bindings to the [buildgrid](https://gitlab.com/BuildGrid/buildgrid/-/blob/master/protos/src/build/buildbox/execution_stats.proto) protos
