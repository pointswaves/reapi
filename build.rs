fn main() {
    let mut build = tonic_build::configure();

    #[cfg(feature = "serde")]
    {
        build = build.type_attribute(
            "build.bazel.remote.execution.v2.Digest",
            "#[derive(serde::Serialize, serde::Deserialize)]",
        );
    }

    #[cfg(feature = "eq")]
    {
        build = build.type_attribute(
            "build.bazel.remote.execution.v2.Digest",
            "#[derive(Eq, Hash)]",
        );
    }

    build
        .compile(
            &[
                "protos/google/protobuf/any.proto",
                "protos/google/protobuf/descriptor.proto",
                "protos/google/protobuf/duration.proto",
                "protos/google/protobuf/empty.proto",
                "protos/google/protobuf/timestamp.proto",
                "protos/google/protobuf/wrappers.proto",
                "protos/google/protobuf/descriptor.proto",
                "protos/google/api/http.proto",
                "protos/google/api/annotations.proto",
                "protos/google/longrunning/operations.proto",
                "protos/build/bazel/remote/execution/v2/remote_execution.proto",
                "protos/build/bazel/remote/asset/v1/remote_asset.proto",
                "protos/build/buildgrid/local_cas.proto",
                "protos/google/bytestream/bytestream.proto",
                "protos/google/rpc/code.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
